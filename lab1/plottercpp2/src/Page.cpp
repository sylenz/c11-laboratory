#include "Page.h"
#include <iostream>

using namespace std;

Page::Page(int w, int h)
    :_w(w), _h(h)
{
    pagedata = vector<vector<char>>(h, vector<char>(w));
}

void Page::setchar(int x, int y, char value) {
    if(y < 0 || y >= _h ||
       x < 0 || x >= _w)
        return; //throw exception.

    pagedata[y][x] = value;
}

char Page::getchar(int x, int y) const{
    return pagedata[y][x];
}

void Page::clear(){
    for(int x = 0; x < _w; x++)
    for(int y = 0; y < _h; y++){
        pagedata[y][x] = ' ';
    }
}


void Page::print(){
    for(int y = 0; y < _h; y++){
        for(int x = 0; x < _w; x++){
            cout << pagedata[y][x];
        }
        cout << endl;
    }
}

int Page::w() const{
    return _w;
}

int Page::h() const{
    return _h;
}

std::ostream& operator<<(std::ostream& os, Page p) {
    for(int y = 0; y < p.h(); y++){
        for(int x = 0; x < p.w(); x++){
            os << p.getchar(x,y);
        }
        os << endl;
    }
    return os;
}
