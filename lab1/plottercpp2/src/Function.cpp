#include "Function.h"
#include <math.h>

SinScaleTimesX::SinScaleTimesX(double scale){
    this->scale = scale;
}
double SinScaleTimesX::operator()(double x){
    return sin(scale * x);
}

double Cos::operator()(double x){
    return cos(x);
}
