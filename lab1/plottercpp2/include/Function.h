#ifndef FUNCTION_H
#define FUNCTION_H

class Function{
public:
    virtual double operator()(double x) = 0;
};

class SinScaleTimesX : public Function{
private:
    double scale;
public:
    SinScaleTimesX(double scale);
    double operator()(double x) override;
};

class Cos : public Function{
public:
    double operator()(double x) override;
};

#endif // FUNCTION_H
