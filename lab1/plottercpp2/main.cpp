#include <iostream>
#include "Function.h"
#include "Page.h"
using namespace std;

//build
//g++ -std::c++11 -Wextra -pedantic -Werror -Wall -o "%e" "%f"
//valgrind
// valgrind --leave-check=full futtathato_fajl

void plot(Page& p, char c, Function& f) {
    for (int x = 0; x < p.w(); ++x) {
        double fx = (x - p.w()/2)/4.0;
        double fy = f(fx);
        int y = (fy * 4.0) * -1 + p.h()/2;
        p.setchar(x,y,c);
    }
}

int main() {

    int H,W;

    cout<<"HxW size, H = (?)\n";
    cin >> H;
    cout<<"HxW size, W = (?)\n";
    cin>>W;

    Page page(W,H);

    double d;
    cout << "sin(d * x), d = ?\n";
    cin >> d;

    SinScaleTimesX f(d);
    Cos c;

    page.clear();
    plot(page, '.', f);
    plot(page, '+', c);
    cout << page;


}
