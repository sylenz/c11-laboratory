#include <stdio.h>
#include <math.h>
#include <ctype.h>
#include <string.h>

void clear(int H, int W, char page[H][W]) {
    for (int y = 0; y < H; ++y)
        for (int x = 0; x < W; ++x)
            page[y][x] = ' ';
}

void plot(int H, int W, char page[H][W], char c, double (*f)(double), double (*transform)(double,void*), void* param) {
    for (int x = 0; x < W; ++x) {
        double fx = (x - W/2)/4.0;
        double fy = f(transform(fx,param));
        int y = (fy * 4.0) * -1 + H/2;
        if (y >= 0 && y < H)
            page[y][x] = c;
    }
}

void print(int H, int W, char page[H][W]) {
    for (int y = 0; y < H; ++y) {
        for (int x = 0; x < W; ++x)
            putchar(page[y][x]);
        putchar('\n');
    }
}

double mult(double x, void* a){
    double d = *(double*)a;
    return d*x;
}

int main() {
    int H,W;

    printf("HxW size, H = (?)\n");
    scanf("%d", &H);
    printf("HxW size, W = (?)\n");
    scanf("%d", &W);

    char page[H][W];

    double d;
    printf("sin(d * x), d = ?\n");
    scanf("%lf", &d);

    clear(H,W, page);
    plot(H,W, page, '.', sin, mult, &d);
    plot(H,W, page, '+', cos, mult, &d);
    print(H,W, page);

}
