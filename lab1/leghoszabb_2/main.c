#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

void setString(char** str, const char* val){
    if(str != NULL){
        free(*str);
    }
    *str = (char*)malloc((strlen(val) + 1) * sizeof(char));
    strcpy(*str, val);
}

void addChar(char** str, const char c){
    size_t len = strlen(*str);
    char* tmp = (char*)malloc((len + 1 + 1) * sizeof(char));
    strcpy(tmp,*str);

    free(*str);
    *str = tmp;

    (*str)[len] = c;
    (*str)[len+1] = '\0';
}

int main()
{
    char c;
    size_t maxlen = 0;
    size_t size = 20;
    char *word, *longest;
    longest = NULL;
    setString(&word, "");

    while ((c = getchar()) != '\n'){
        if(isalpha(c)){
            addChar(&word, c);
        }else{
            if(strlen(word) > maxlen){
                maxlen = strlen(word);
                setString(&longest, word);
            }
            setString(&word, "");
        }
    }

    if(strlen(word) > maxlen){
        maxlen = strlen(word);
        setString(&longest, word);
    }

    printf("The longest word is \"%s\"", longest);
    free(word);
    free(longest);
}
