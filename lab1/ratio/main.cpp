#include <iostream>
#include "Ratio.h"

using namespace std;

//Itt találtam egy ismerős osztályt a példakódnál(alul) :)
//http://en.cppreference.com/w/cpp/language/operators

int main()
{
    Ratio r(5,2);
    Ratio r2(1,2);
    cout << r << endl;
    cin >> r;
    cout << r << endl;
    r2 += r;
    cout << r2 << " = " << (double)r2 << endl;

    Ratio r3 = -r;
    Ratio r4 = +r;

    cout << r3 << endl;
    cout << r4 << endl;
    return 0;
}
