#include "Ratio.h"

int Ratio::num() const { return num_; }
int Ratio::den() const { return den_; }

Ratio::Ratio(int num, int den) {
    /* Euclidean: gcd -> a */
    int a = num, b = den, t;
    while (b != 0)
        t = a%b, a = b, b = t;
    num_ = num/a;
    den_ = den/a;
}


Ratio::operator double() const{
    double n = num_;
    return n/den_;
}


Ratio operator+(Ratio r1, Ratio r2) {
    return Ratio(
        r1.num() * r2.den() + r2.num() * r1.den(),
        r1.den() * r2.den());
}

Ratio operator-(Ratio r1, Ratio r2) {
    return r1 + (Ratio(-1) * r2);
}


Ratio operator+(Ratio r){
    return r;
}


Ratio operator-(Ratio r){
    return Ratio(-r.num(), r.den());
}

Ratio operator*(Ratio r1, Ratio r2) {
    return Ratio(
        r1.num() * r2.num(),
        r1.den() * r2.den());
}

Ratio operator/(Ratio r1, Ratio r2) {
    return Ratio(
        r1.num() * r2.den(),
        r1.den() * r2.num());
}

Ratio& operator+=(Ratio& r1,const Ratio& r2){
    r1 = r1 + r2;
    return r1;
}

Ratio& operator-=(Ratio& r1,const Ratio& r2){
    r1 = r1 - r2;
    return r1;
}

Ratio& operator*=(Ratio& r1,const Ratio& r2){
    r1 = r1 * r2;
    return r1;
}

Ratio& operator/=(Ratio& r1,const Ratio& r2){
    r1 = r1 / r2;
    return r1;
}

std::ostream &operator<<(std::ostream &os, Ratio r) {
    os << r.num() << '/' << r.den();
    return os;
}

std::istream &operator>>(std::istream &is, Ratio& r){
    int num, den;
    char dummy;
    is >> num >> dummy >> den;
    r = Ratio(num,den);
    return is;
}
