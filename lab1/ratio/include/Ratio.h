#ifndef RATIO_H
#define RATIO_H
#include <iostream>

class Ratio {       /* rational number */
  public:
    Ratio(int num, int den = 1);
    int num() const;
    int den() const;

    operator double() const;

  private:
    int num_;       /* numerator */
    int den_;       /* denominator */
};
Ratio operator+(Ratio r1, Ratio r2);
Ratio operator+(Ratio r);
Ratio operator-(Ratio r);
Ratio operator-(Ratio r1, Ratio r2);
Ratio operator*(Ratio r1, Ratio r2);
Ratio operator/(Ratio r1, Ratio r2);


Ratio& operator+=(Ratio& r1,const Ratio& r2);
Ratio& operator-=(Ratio& r1,const Ratio& r2);
Ratio& operator*=(Ratio& r1,const Ratio& r2);
Ratio& operator/=(Ratio& r1,const Ratio& r2);
std::ostream &operator<<(std::ostream &os, Ratio r);
std::istream &operator>>(std::istream &is, Ratio& r);
#endif // RATIO_H
