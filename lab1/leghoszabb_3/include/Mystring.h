#ifndef MYSTRING_H
#define MYSTRING_H
#include <iostream>

class Mystring
{
    public:
        Mystring();
        Mystring(const char* val);

        ~Mystring();

        Mystring& operator+=(const char& c);
        Mystring& operator=(const char* val);
        Mystring& operator=(const Mystring& val);

        size_t length() const;
        const char* getString() const;

    private:
        char* str;
};


std::ostream& operator<<(std::ostream& os, const Mystring& ms);

#endif // MYSTRING_H
