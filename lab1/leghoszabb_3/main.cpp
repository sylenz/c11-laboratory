#include <iostream>
#include "Mystring.h"

using namespace std;

int main()
{
    char c;
    Mystring word, longest;
    while ((c = getchar()) != '\n'){
        if(isalpha(c)){
            word += c;
        }else{
            if(word.length() > longest.length()){
                longest = word;
            }
            word = "";
        }
    }

    if(word.length() > longest.length()){
        longest = word;
    }

    cout << "The longest word is \"" << longest << "\"" << endl;
}
