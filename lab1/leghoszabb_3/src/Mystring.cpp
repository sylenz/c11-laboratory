#include "Mystring.h"
#include <string.h>

Mystring::Mystring()
{
    str = new char[1];
    str[0] = '\0';
}

Mystring::Mystring(const char* val){
    str = new char[strlen(val) + 1];
    strcpy(str, val);
}

Mystring::~Mystring()
{
    delete[] str;
    str = nullptr;
}



Mystring& Mystring::operator+=(const char& c){
    size_t len = strlen(str);
    char* tmp = new char[len + 2];
    strcpy(tmp, str);
    delete[] str;
    str = tmp;
    str[len] = c;
    str[len + 1] = '\0';
    return *this;
}

Mystring& Mystring::operator=(const char* val){
    delete[] str;
    str = new char[strlen(val) + 1];
    strcpy(str, val);
    return *this;
}


Mystring& Mystring::operator=(const Mystring& that){
    delete[] str;
    str = new char[strlen(that.str) + 1];
    strcpy(str, that.str);
    return *this;
}

const char* Mystring::getString() const{
    return str;
}

size_t Mystring::length() const{
    return strlen(str);
}


std::ostream& operator<<(std::ostream& os, const Mystring& ms) {
    os << ms.getString();
    return os;
}
