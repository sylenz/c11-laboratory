#ifndef PAGE_H
#define PAGE_H
#include <vector>
#include <iostream>

class Page{
public:
    Page(int w, int h);

    void setchar(int x, int y, char value);
    char getchar(int x, int y) const;

    int w() const;
    int h() const;

    void clear();
    void print();

private:
    int _w;
    int _h;

    std::vector<std::vector<char>> pagedata;
};

std::ostream& operator<<(std::ostream& os, Page p);
#endif // PAGE_H
