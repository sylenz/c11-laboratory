#include <stdio.h>
#include <stdlib.h>

int main()
{
    char c;
    size_t maxlen = 0;
    size_t size = 20;
    char *word, *longest;
    word = (char*)malloc(size * sizeof(char));
    longest = (char*)malloc(size * sizeof(char));
    word[0] = '\0';
    longest[0] = '\0';
    while ((c = getchar()) != '\n'){
        if(isalpha(c)){
            size_t len = strlen(word);
            if(len >= size-1){
                char* tmp = (char*)malloc(size * 2 * sizeof(char));
                strcpy(tmp, word);
                free(word);
                word = tmp;
            }
            word[len] = c;
            word[len+1] = '\0';

        }else{
            if(strlen(word) > maxlen){
                maxlen = strlen(word);
                char *tmp = longest;
                longest = word;
                word = tmp;
            }
            word[0] = '\0';
        }
    }

    if(strlen(word) > maxlen){
        maxlen = strlen(word);
        char *tmp = longest;
        longest = word;
        word = tmp;
    }

    printf("The longest word is \"%s\"", longest);
    free(word);
    free(longest);
}
