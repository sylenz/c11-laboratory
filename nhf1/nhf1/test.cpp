#include <iostream>
#include "mystring.h"

using namespace std;

#define line() std::cout << "----------" << std::endl;
#define myassert(a,b) std::cout << "expecting: " << a << " actual: " << b << std::endl;

int main()
{
    MyString x("hello"); //const char* ctor
    MyString y = x; //copy ctor
    MyString z; //default ctor

    line();
    myassert(5, x.size());
    myassert(0, z.size());

    line();
    x += y; //operator += Mystring rhs
    myassert("hellohello", x);

    line();
    x = y;
    y[0] = 'c'; //indexing copy-on-write
    myassert("cello", y);
    line();
    z = y; //operator =
    myassert("cello", z);

    line();
    z += 'a'; // += char rhs
    myassert("celloa", z);

    line();
    z += z; // += same Mystring rhs
    myassert("celloacelloa", z);

    line();
    z = z; //dont crash
    myassert("celloacelloa", z);

    line();
    z = x + y; //operator + and move assignment operator
    myassert("hellocello", z);

    line();
    MyString w = x + y + 'W'; //operator + Mystring and char rhs, move ctor
    myassert("hellocelloW", w);

    line();
    cout << "Írj be valami szöveget: ";
    cin >> w;
    cout << "beolvasott szöveg: " << w << endl;

    line();
    MyString u = w;
    myassert(2, u.use_count());//ha nem x,y,z egyikét írjuk be akkor igaz csak.
    if(u.size() > 0){
        cout << "Olvasás: " << u[0] << endl; //no copy
        cout << "Írás:" << endl;
        u[0] = w[0] == 'Q' ? 'A' : 'Q';//copy-on-write
        myassert(1, u.use_count());
        u[0] = u[0] + 1; //new instance of stringvalue
        myassert(1, u.use_count());
        u[0] = w[0];
        myassert(2, u.use_count());
    }
    line();

    return 0;
}
