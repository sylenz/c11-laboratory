#include "mystring.h"
#include <cstring>
#include <exception>
#include <iostream>
#include <map>

//For RAII without std::unique_ptr
template<typename T>
class SimplePtr {
private:
    T* _ptr;
public:
    SimplePtr(T* t = nullptr)
        :_ptr(t) {}

    ~SimplePtr(){
        delete[] _ptr;
    }

    SimplePtr(const SimplePtr<T>&) = delete;
    void operator=(const SimplePtr<T>&) = delete;

    T* get() const{
        return _ptr;
    }
    operator T*() const{
        return _ptr;
    }
};


class StringValue{
public:
    StringValue(const StringValue&) = delete;
    void operator=(const StringValue&) = delete;

    StringValue(const char * c)
        :refCount(0) {
        c_ptr = new char[strlen(c) + 1];
        strcpy(c_ptr, c);
    }


    ~StringValue(){
        delete[] c_ptr;
    }

    bool hasReference() const noexcept{
        return refCount > 0;
    }

    void release() noexcept{
        refCount--;
    }

    void acquire() noexcept{
        refCount++;
    }

    unsigned int use_count() const noexcept{
        return refCount;
    }

    char* c_ptr;
private:
    unsigned int refCount;
};


class StringValueManager{
private:
    //https://stackoverflow.com/questions/4157687/using-char-as-a-key-in-stdmap
    struct cmp_str
    {
       bool operator()(const char *a, const char *b)
       {
          return std::strcmp(a, b) < 0;
       }
    };

    StringValueManager() = default;
    using StringValueMap = std::map<const char*, StringValue*, cmp_str>;
    StringValueMap stringValues;
public:

    static StringValueManager& getInstance(){
        static StringValueManager instance;
        return instance;
    }

    StringValue* getString(const char* str){
        if(str == nullptr)
            return nullptr;

        auto it = stringValues.find(str);
        if(it != stringValues.end()){
            //already exists
            return it->second;
        }
        else{
            //new value
            auto stringValue = new StringValue(str);
            stringValues[stringValue->c_ptr] = stringValue;
            return stringValue;
        }
    }

    bool exists(const char* str){
        if(str == nullptr)
            return false;

        auto it = stringValues.find(str);
        return it != stringValues.end();
    }

    void releaseStringValue(const char* str){
        auto it = stringValues.find(str);
        if(it != stringValues.end()){
            StringValue* sv = it->second;
            stringValues.erase(it);
            delete sv;
        }
    }

    StringValue* remove(const char* str){
        auto it = stringValues.find(str);
        if(it != stringValues.end()){
            StringValue* sv = it->second;
            stringValues.erase(it);
            return sv;
        }
        else{
            return nullptr;
        }
    }

    void add(StringValue* sv){
        if(sv == nullptr)
            return;

        stringValues[sv->c_ptr] = sv;
    }

};

///MyString

///Constructors
MyString::MyString(const char * str){
    acquire(StringValueManager::getInstance().getString(str));
}

MyString::MyString(const MyString& other){
    acquire(other.stringValue);
}

MyString::MyString(MyString&& other) noexcept{
    stringValue = other.stringValue;
    other.stringValue = nullptr;
}

MyString::~MyString() {
    release();
}

MyString& MyString::operator=(const MyString& other){
    if(this != &other){
        release();
        acquire(other.stringValue);
    }
    return *this;
}

MyString& MyString::operator=(MyString&& other) noexcept {
    if(this != &other){
        release();
        stringValue = other.stringValue;
        other.stringValue = nullptr;
    }
    return *this;
}

MyString& MyString::operator=(const char* str){
    release();
    acquire(StringValueManager::getInstance().getString(str));
    return *this;
}


///public methods
const char* MyString::getString() const {
    return stringValue->c_ptr;
}

size_t MyString::size() const{
    //Work with empty MyString.
    if(stringValue)
        return strlen(stringValue->c_ptr);
    else
        return 0;
}


const char& MyString::operator[](size_t idx) const{
    if(idx >= size())
        throw std::out_of_range("");
    return stringValue->c_ptr[idx];
}

MyString::CharacterProxy MyString::operator[](size_t idx) {
    if(idx >= size())
        throw std::out_of_range("");
    return CharacterProxy(*this, idx);
}

void MyString::setChar(char c, size_t idx){
    if(idx >= size())
        throw std::out_of_range("");
    if(stringValue->c_ptr[idx] == c)
        return;

    if(stringValue->use_count() > 1){
        std::cout << "Data copied on write" << std::endl;
        SimplePtr<char> data(new char[size() + 1]);
        strcpy(data.get(), stringValue->c_ptr);
        release();
        data[idx] = c;
        acquire(StringValueManager::getInstance().getString(data.get()));
    }
    else {
        //Hm, there's two different goals that collide here.
        //On one side we want that if this is the only object using this string
        //we use the same array as before and expect this method to be fast.
        //On the other hand we don't want duplicated strings.
        //If I do a copy to a local char array, then I can easily check if the changed string exists,
        //but make this method slow.
        //If I do not copy then I need to use this SV's char array and change it back and forth,
        //since changing the char array will break the map that the SVM is using, because I'm changing
        //the key value behind it.

        //TLDR: We want this to be fast, but there's only a slow method that can check
        //if the changed version already exists.

        //I choose the slow version, otherwise the SVM should be rewritten.

        SimplePtr<char> changed(new char[size() + 1]);
        strcpy(changed.get(), stringValue->c_ptr);
        changed[idx] = c;

        if(StringValueManager::getInstance().exists(changed.get())){
            release();
            acquire(StringValueManager::getInstance().getString(changed.get()));
        }
        else{
            StringValueManager::getInstance().remove(stringValue->c_ptr);
            stringValue->c_ptr[idx] = c;
            StringValueManager::getInstance().add(stringValue);
        }
    }
}

char MyString::getChar(size_t idx) const{
    if(idx >= size())
        throw std::out_of_range("");

    return stringValue->c_ptr[idx];
}

size_t MyString::use_count() const{
    return stringValue->use_count();
}

MyString& MyString::operator+=(const MyString& other){
    if(stringValue){
        //RAII
        SimplePtr<char> combined(new char[size() + other.size() + 1]);
        combined[0] = '\0';
        strcat(combined.get(), getString());
        strcat(combined.get(), other.getString());
        release();
        acquire(StringValueManager::getInstance().getString(combined.get()));
    }
    else{
        //addref to other's manager
        acquire(other.stringValue);
    }


    return *this;
}

MyString& MyString::operator+=(const char c){
    //RAII
    SimplePtr<char> combined(new char[size() + 2]);
    if(stringValue)
        strcpy(combined.get(), getString());

    combined[size()] = c;
    combined[size() + 1] = '\0';

    release();
    acquire(StringValueManager::getInstance().getString(combined.get()));
    return *this;
}

///private methods
void MyString::release() {
    if(stringValue){
        stringValue->release();
        if(!stringValue->hasReference()){
            StringValueManager::getInstance().releaseStringValue(stringValue->c_ptr);
        }
        stringValue = nullptr;
    }
}

void MyString::acquire(StringValue* strM){
    stringValue = strM;
    if(stringValue){
        stringValue->acquire();
    }
}

///global methods
std::ostream& operator<<(std::ostream& os, const MyString& str){
    os << str.getString();
    return os;
}


std::istream& operator>>(std::istream& is, MyString& ms){
    ms = "";
    constexpr int buffersize = 100;
    char c[buffersize];
    do {
        //unset failbit if bigger line than buffer
        is.clear();
        is.getline(c, buffersize);

        ms += c;

    } while(is.fail() && !is.bad());
    return is;
}


MyString operator+(const MyString& lhs, const MyString& rhs){
    MyString newStr = lhs;
    newStr += rhs;
    return newStr;
}
MyString operator+(const MyString& lhs, const char c){
    MyString newStr = lhs;
    newStr += c;
    return newStr;
}

//Character Proxy

MyString::CharacterProxy::CharacterProxy(MyString& s, size_t idx)
    :s{s}, idx{idx} {}

MyString::CharacterProxy::operator char() const{
    return s.getChar(idx);
}

char MyString::CharacterProxy::operator=(const char c2){
    s.setChar(c2, idx);
    return c2;
}


MyString::CharacterProxy& MyString::CharacterProxy::operator=(const MyString::CharacterProxy& other){
    s.setChar(static_cast<char>(other), idx);
    return *this;
}


