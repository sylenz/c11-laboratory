#pragma once
#include <iostream>

//Don't want this class to be public at all, but needed for header file.
//It could be a private class actually (pimpl)
class StringValue;


class MyString final
{
public:

    class CharacterProxy{
    private:
        MyString& s;
        size_t idx;

        CharacterProxy(MyString& s, size_t idx);
        friend MyString;
    public:

        CharacterProxy& operator=(const CharacterProxy& other);

        operator char() const;
        char operator=(const char c2);
    };

    MyString(const char *c = "");
    MyString(const MyString&);
    MyString(MyString&&) noexcept;

    ~MyString() noexcept;

    MyString& operator=(const MyString&);
    MyString& operator=(MyString&&) noexcept;
    MyString& operator=(const char* str);

    MyString& operator+=(const MyString&);
    MyString& operator+=(const char);

    size_t size() const;
    CharacterProxy operator[](size_t idx);
    //When const it is unnecessary to use proxy.
    const char& operator[](size_t idx) const;
    const char* getString() const;
    void setChar(char c, size_t idx);
    char getChar(size_t idx) const;

    size_t use_count() const;

private:
    void release();
    void acquire(StringValue*);
    StringValue* stringValue = nullptr;
};

std::ostream& operator<<(std::ostream&, const MyString&);
std::istream& operator>>(std::istream&, MyString&);

MyString operator+(const MyString&, const MyString&);
MyString operator+(const MyString&, const char);
