/***
Meg fogok tobb megoldason gondolkozni, ez csak teszt beadas, hogy kerdesre kapjak valaszt.
Amit a hanoi fuggvenyben modositottam az engedett-e?
Hogy lehetne a masodik operator<< fv-ben kiszurni, hogy endl-t kaptam-e?
***/


#include <iostream>

using namespace std;

class mycout {
private:
    ostream& os;
    int counter = 1;
    bool needLineStart = true;
public:
    mycout(ostream& os) : os{os} {}

    template <typename T>
    mycout& operator<< (const T& t) {

        if(needLineStart){
            os << counter << ". lepes: ";
            needLineStart = false;
        }

        os << t;
        return *this;
    }

    mycout& operator<< (std::ostream& (*f)(std::ostream &)) {
        //Assumes endl :(
        //No idea atm how to check it.
        counter++;
        needLineStart = true;

        f(std::cout);
        return *this;
    }
} myout(std::cout);


void hanoi(int n, char honnan, char seged, char hova) {
    if (n == 0)
        return;
    hanoi(n-1, honnan, hova, seged);
    myout << "rakj 1-et: " << honnan << "->" << hova << std::endl;
    hanoi(n-1, seged, honnan, hova);
}



int main() {
    hanoi(4, 'A', 'B', 'C');

    return 0;
}
