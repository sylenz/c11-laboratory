#include <iostream>
#include <stdexcept>
#include <string>

using namespace std;

template<typename T>
class Optional{
private:
    bool hasValue = false;
    //Fix 1 byte length type... I hope it is.
    alignas(alignof(T))
    char valarray[sizeof(T)];

public:
    Optional() = default;
    Optional(const T& t)
        :hasValue{true}{
        new (valarray) T{t};
    }

    Optional(const Optional& that)
        :hasValue{that.hasValue} {
        if(that.hasValue)
            new (valarray) T{*that};
    }

    Optional& operator=(const Optional& that) {
        if(hasValue)
            ((T*)valarray)->~T();

        hasValue = that.hasValue;
        if(that.hasValue)
            new (valarray) T{*that};

        return *this;
    }

    ~Optional(){
        if(hasValue)
            ((T*)valarray)->~T();
    }

    explicit operator bool() const{
        return hasValue;
    }

    T& operator *() {
        if(!hasValue)
            throw std::exception();
        return *((T*)valarray);
    }

    const T& operator *() const {
        if(!hasValue)
            throw std::exception();

        return *((T*)valarray);
    }
};

Optional<int> get_number_from_stdin(){
    int i;
    if(cin >> i)
        return Optional<int>{i};
    else
        return Optional<int>{};
}

int main()
{
    Optional<int> opt = get_number_from_stdin();
    if(opt){
        cout << "Ezt a számot kaptam: " << *opt << endl;
    }
    else{
        cout << "Nem kaptam számot" << endl;
    }

    return 0;
}
