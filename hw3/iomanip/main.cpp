#include <iostream>
#include <string>

using namespace std;

class BinaryPrinter{
private:
    ostream& os;
public:
    BinaryPrinter(ostream& os)
        :os(os){}

    ostream& operator<<(unsigned int a){
        string binary;
        while(a > 0){
            if(a % 2 == 1)
                binary = "1" + binary;
            else
                binary = "0" + binary;

            a /= 2;
        }
        if(binary.length() == 0)
            binary = "0";

        os << binary;
        return os;
    }
};

class BinaryManipulator {};


BinaryPrinter operator<<(ostream& os, BinaryManipulator&){
    return BinaryPrinter{os};
}

class BasePrinter{
private:
    ostream& os;
    unsigned int base;
public:
    BasePrinter(ostream& os, unsigned int base)
        :os(os), base{base} {}

    ostream& operator<<(unsigned int a){
        string coded;
        while(a > 0){
            unsigned int rem = a % base;
            if(rem < 10)
                coded = static_cast<char>('0' + rem) + coded;
            else
                coded = static_cast<char>('A' + (rem - 10)) + coded;

            a /= base;
        }
        if(coded.length() == 0)
            coded = "0";

        os << coded;
        return os;
    }
};

class base{
private:
    unsigned int val;
public:
    base(unsigned int b)
        :val{b} {}
    unsigned int b() const { return this->val; }
};

BasePrinter operator<<(ostream& os, base b){
    return BasePrinter{os, b.b()};
}

int main()
{
    BinaryManipulator bin;
    unsigned int i = 240;
    cout << bin << i << endl;
    cout << base(2) << i << endl;
    cout << base(16) << i << endl;
    return 0;
}
