#include <iostream>
#include "EvilHangmanGame.h"
#include "iohelpers.h"
#include <map>

using namespace std;

int main()
{

    std::vector<string> words = {
        "test", "word", "list", "asd", "two", "aaa", "aa", "a"
    };
    EvilHangmanGame g;
//    g.loadFromVector(words);
    g.loadFromFile("resources/dictionary.txt");
    bool playagain;
    do{
        g.run();
        playagain = getYesNo("Would you like to play again?");
    } while(playagain);
    return 0;
}
