#ifndef IOHELPERS_H
#define IOHELPERS_H
#include <iostream>
#include <string>

unsigned int getUInt(const std::string& statement = "");
char getLetter(const std::string& statement = "");
bool getYesNo(const std::string& statement = "");

#endif // IOHELPERS_H
