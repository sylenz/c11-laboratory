#ifndef EVILHANGMANGAME_H
#define EVILHANGMANGAME_H

#include <string>
#include <vector>
#include <iostream>
#include <map>

class EvilHangmanGame
{
    private:
        std::vector<std::string> dictionary;
        std::vector<std::string> currentWords;
        std::vector<char> usedLetters;
        std::string guess;
        unsigned int life = 6;
    public:

        /** Loads dictionary from the given file.
         */
        void loadFromFile(const std::string& file);

        /** Uses the given words as dictionary.
         */
        void loadFromVector(const std::vector<std::string>& words);

        /** Automatically plays the game using standard in and output.
         */
        void run();

        void printGamestate(std::ostream& os) const;

    private:
        void chooseWordLength(size_t length);

        void chooseLetter(char c);

        std::multimap<std::string,std::string> createWordGroups(char letter) const;

        bool gameOver() const;
};

std::ostream& operator<<(std::ostream& os, EvilHangmanGame& game);
#endif // EVILHANGMANGAME_H
