#include "iohelpers.h"
#include <algorithm>
#include <string>

using namespace std;

char getLetter(const std::string& statement){
    string input;
    do {
        cout << statement;
        cin >> input;
    } while(input.length() != 1 && !isalpha(input[0]));

    return input[0];
}


unsigned int getUInt(const std::string& statement){
    string userinput;
    bool all_digits;
    do {
        cout << statement;
        cin >> userinput;
        all_digits = std::all_of(userinput.begin(), userinput.end(),
            [](char c) {return std::isdigit(c);});
    } while(!all_digits);
    return std::stoul(userinput);
}

bool getYesNo(const std::string& statement){
    string userinput;
    do {
        cout << statement << " (y/n)";
        cin >> userinput;
        std::transform(userinput.begin(), userinput.end(), userinput.begin(), ::tolower);
    } while(userinput != "y"s && userinput != "yes"s &&
            userinput != "n"s && userinput != "no"s);

    return userinput == "y"s || userinput == "yes"s ;

}
