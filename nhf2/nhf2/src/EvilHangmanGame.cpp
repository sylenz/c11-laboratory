#include "EvilHangmanGame.h"
#include "iohelpers.h"
#include <algorithm>
#include <map>
#include <fstream>

using namespace std;

//https://stackoverflow.com/a/771482
//Solution for iterating through map with transform.
//Really simple, but I looked up the solution before thought about how I can do this.
//So have to give credit, where credit is due.
template<typename tPair>
struct second_t {
    typename tPair::second_type operator()( const tPair& p ) const {
        return p.second;
    }
};

template<typename tMap>
second_t<typename tMap::value_type> second( const tMap& ) {
    return second_t<typename tMap::value_type>();
}


void EvilHangmanGame::run(){
    if(dictionary.size() == 0){
        cout << "No words are loaded." << endl;
        return;
    }

    currentWords.clear();
    usedLetters.clear();

    while(currentWords.size() == 0){
        unsigned int wordLength = getUInt("Choose a word length ");
        if(wordLength > 0){
            chooseWordLength(wordLength);
        }
    }

    life = getUInt("How many lifes do you need? ");
    bool writeEvilInfo = getYesNo("Would you like to see how many words are possible?");
    while(!gameOver()){

        cout << endl << endl;
        printGamestate(cout);
        if(writeEvilInfo)
            cout << currentWords.size() << " word(s) are left." << endl;

        char chosenLetter = getLetter("Choose a letter ");

        if(find(usedLetters.begin(),usedLetters.end(), chosenLetter) != usedLetters.end()){
            cout << "Letter already used." << endl;
            continue;
        }

        chooseLetter(chosenLetter);
    }
    printGamestate(cout);
    if(life > 0)
        cout << "You won!" << endl;
    else {
        cout << "You died" << endl;
        cout << "The word was: " << currentWords[0] << endl;
    }
}

bool EvilHangmanGame::gameOver() const{
    return life == 0 || //loose
           (currentWords.size() == 1 && currentWords[0] == guess); //win
}

void EvilHangmanGame::chooseWordLength(size_t length){
    currentWords.clear();

    std::copy_if(dictionary.begin(), dictionary.end(), std::back_inserter(currentWords),
                 [=](string word){
                    return word.length() == length;
                 });

    guess = string(length, '_');
}

void EvilHangmanGame::chooseLetter(char c){
    if(!isalpha(c))
        return;

    c = tolower(c);

    multimap<string,string> wordGroups = createWordGroups(c);

    //find biggest group
    string mostFrequentGroup;
    size_t maxSize = 0;
    for(auto it = wordGroups.begin(); it != wordGroups.end();
        it = wordGroups.upper_bound(it->first)){

        size_t count = wordGroups.count(it->first);
        if(count > maxSize){
            maxSize = count;
            mostFrequentGroup = it->first;
        }
    }

    //select biggest group
    currentWords.clear();
    currentWords.reserve(maxSize);

    std::transform(wordGroups.lower_bound(mostFrequentGroup),
                   wordGroups.upper_bound(mostFrequentGroup),
                   std::back_inserter(currentWords),
                   second(wordGroups));

    //update guess
    for(size_t i = 0; i < mostFrequentGroup.length(); i++){
        if(mostFrequentGroup[i] == '1')
            guess[i] = c;
    }

    //update life
    if(mostFrequentGroup == string(mostFrequentGroup.length(), '0'))
        life--;

    usedLetters.push_back(c);
}

multimap<string,string> EvilHangmanGame::createWordGroups(char letter) const{

    multimap<string,string> wordGroups;
    for(auto& word : currentWords){
        string groupKey(word.length(), '0');

        for(size_t i = 0; i < word.length(); i++){
            if(word[i] == letter)
                groupKey[i] = '1';
        }

        wordGroups.insert(make_pair(groupKey, word));
    }
    return wordGroups;
}


void EvilHangmanGame::printGamestate(ostream& os) const{

    os << "_________ \n";
    os << "|       | \n";
    os << "|       O \n";
    if(life < 1){
        os << "|\n";
        os << "|\n";
    }
    else if(life == 1){
        os << "|       |\n";
        os << "|\n";
    }
    else if(life == 2){
        os << "|      /|\n";
        os << "|\n";
    }
    else if(life == 3){
        os << "|      /|\\\n";
        os << "|\n";
    }
    else if(life == 4){
        os << "|      /|\\\n";
        os << "|      /  \n";

    }
    else{
        os << "|      /|\\\n";
        os << "|      / \\\n";
    }

    os << "|\n";
    os << endl;
    os << "Remaining life: " << life << endl;
    for(auto c : guess)
        os << c << " ";
    os << endl;
}

void EvilHangmanGame::loadFromFile(const string& filepath) {
    dictionary.clear();
    ifstream dictionaryFile(filepath);
    string word;
    while(std::getline(dictionaryFile,word)){
        dictionary.push_back(word);
    }
}

void EvilHangmanGame::loadFromVector(const vector<string>& words){
    dictionary = words;
}

std::ostream& operator<<(std::ostream& os, EvilHangmanGame& game){
    game.printGamestate(os);
    return os;
}
